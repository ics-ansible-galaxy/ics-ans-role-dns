ics-ans-role-dns
===================

Ansible role to install ics-ans-role-dns.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

Modify defaults/main.yml
```yaml
bind_zone_master_server_ip: DNSSRVIPADDRESS
bind_zone_networks:
  - "172.16.1"
  - "172.16.2"
...
bind_zone_hosts:
  - name: ns
    ip: 172.16.2.200
  - name: "switch-01"
    ip: 172.16.2.10
    aliases:
      - test1
  - name: test
    ip: 172.16.2.11
  - name: dc01
    ip: 10.0.42.21
  - name: dc02
    ip: 10.0.42.22
  - name: dc03
    ip: 10.0.42.23

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-dns
```

License
-------

BSD 2-clause
