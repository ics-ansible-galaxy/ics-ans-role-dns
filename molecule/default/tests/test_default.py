import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_bind_enabled_and_started(host):
    service = host.service("named")
    assert service.is_running
    assert service.is_enabled
